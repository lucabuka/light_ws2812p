/*
* -- WS2812P lib v1.0 - 2020-04-06 
* -- Author: Gitlab @lucabuka
* --     Modified version of "light weight WS2812 lib V2.1" by  Tim (cpldcpu@gmail.com)
* --
* --  The original lib was modified to use a user-defined Palette 
* --  The 'Color value' for each Pixel (LED) is stored in ONE BYTE 
* --  while the original library uses 3 Bytes.  
* -- 
* --     Pros:
* --        - You can manage more LEDs with the same memory amount
* --
* --     Cons:
* --        - Full 32-bit color set NOT available. You need to define
* --           a "Palette" with the colors you will use
* --        -  Only GRB mode available 
* --
* --     This library was developed specifically to manage 600 LED with 
* --     an Atmel ATmega328P (Arduino Nano)
* --
*
* light weight WS2812 lib V2.1 - Arduino support
*
* Controls WS2811/WS2812/WS2812B RGB-LEDs
* Author: Matthias Riegler
*
* Mar 07 2014: Added Arduino and C++ Library
*
* September 6, 2014:	Added option to switch between most popular color orders
*						(RGB, GRB, and BRG) --  Windell H. Oskay
* 
* License: GNU GPL v2 (see License.txt)
*/

#include "WS2812P.h"
#include <stdlib.h>

WS2812P::WS2812P(uint16_t num_leds) {
	count_led = num_leds;
	pixels = (uint8_t*)malloc(count_led);
}


void WS2812P::setPalette(uint8_t *palette_ptr) {
	palette = palette_ptr;
}

uint8_t WS2812P::setPixelColorId(uint16_t index, uint8_t pId) {
	if(index < count_led) {
		pixels[index] = pId;
		return 0;
	} 
	return 1;
}

uint8_t WS2812P::getPixelColorId(uint16_t index) {
	if(index < count_led) {
        return(pixels[index]);
    }
    return(0xFF);
}

void WS2812P::sync() {
	*ws2812_port_reg |= pinMask; // Enable DDR
	ws2812_sendarray_mask(pixels,count_led,pinMask,(uint8_t*) ws2812_port,(uint8_t*) ws2812_port_reg );	
}


WS2812P::~WS2812P() {
	free(pixels);
	
}

#ifndef ARDUINO
void WS2812P::setOutput(const volatile uint8_t* port, volatile uint8_t* reg, uint8_t pin) {
	pinMask = (1<<pin);
	ws2812_port = port;
	ws2812_port_reg = reg;
}
#else
void WS2812P::setOutput(uint8_t pin) {
	pinMask = digitalPinToBitMask(pin);
	ws2812_port = portOutputRegister(digitalPinToPort(pin));
	ws2812_port_reg = portModeRegister(digitalPinToPort(pin));
}
#endif 
