light_ws2812_P  -   V1.0
--------------


WS2812P lib v1.0 - 2020-04-06 
Author: Gitlab @lucabuka

* Modified version of **light weight WS2812 lib V2.1 by  Tim (cpldcpu@gmail.com)**

  The original lib was modified to use a user-defined Palette.
  The 'Color value' for each Pixel (LED) is **stored in ONE BYTE ** while the original library uses 3 Bytes.

     Pros:
        - You can manage more LEDs with the same memory amount

     Cons:
        - Full 32-bit color set NOT available. You need to define
           a "Palette" with the colors you will use
        - Only GRB mode available 

     This library was developed specifically to manage 600 LED with an Atmel ATmega328P (Arduino Nano)
     Our application uses 9 different colors in the Palette


This is the original Tim's lib" 
- https://github.com/cpldcpu/light_ws2812
