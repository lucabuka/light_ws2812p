#################################
# Syntax Coloring Map For WS2812P
#################################
# Class
#######################################

WS2812P	KEYWORD1

#######################################
# Methods and Functions
#######################################	

setOutput		KEYWORD2
setPalette		KEYWORD2
sync			KEYWORD2
setPixelColorId	KEYWORD2
getPixelColorId	KEYWORD2


